Examples
========

Below we provide example scripts, which set up simple synthetic models, for
demonstration purposes.

They can also be found in the `examples
<https://gitlab.com/deltares/imod/imod-python/tree/master/examples>`_ directory
of the repository.

:doc:`api/mf6`

.. toctree::
    :maxdepth: 2

    examples/twri.rst

:doc:`api/wq`

.. toctree::
    :maxdepth: 2

    examples/elder.rst
    examples/freshwaterlens.rst
    examples/henrycase.rst
    examples/hydrocoin.rst
    examples/saltwaterpocket.rst
    examples/verticalinterface.rst
