imod.mf6  -  Create Modflow 6 model
-----------------------------------

.. automodule:: imod.mf6
    :members:
    :imported-members:
    :undoc-members:
    :show-inheritance:

.. automodule:: imod.mf6.model
    :members: Model
    :undoc-members:
    :show-inheritance:

.. automodule:: imod.mf6.pkgbase
    :members:
    :show-inheritance:
