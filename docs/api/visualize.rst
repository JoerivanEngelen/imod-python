imod.visualize  -  Customized plots
-----------------------------------

.. automodule:: imod.visualize
    :members:
    :imported-members:
    :undoc-members:
    :show-inheritance:
