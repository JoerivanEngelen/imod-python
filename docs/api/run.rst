imod.run  -  Write iMODFLOW Runfiles
------------------------------------

.. automodule:: imod.run
    :members:
    :undoc-members:
    :show-inheritance:
