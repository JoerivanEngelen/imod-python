imod.util  -  Miscellaneous Utilities
-------------------------------------

.. automodule:: imod.util
    :members:
    :undoc-members:
    :show-inheritance:
