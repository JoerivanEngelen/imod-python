imod.evaluate -- Evaluate model output
--------------------------------------

.. automodule:: imod.evaluate
    :members:
    :imported-members:
    :undoc-members:
    :show-inheritance:
