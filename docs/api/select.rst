imod.select  -  Get points and cross sections
---------------------------------------------

.. automodule:: imod.select
    :members:
    :imported-members:
    :undoc-members:
    :show-inheritance:
