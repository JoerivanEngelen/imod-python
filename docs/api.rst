API Reference
=============

.. toctree::
    :maxdepth: 1
    :caption: File input/output

    api/idf.rst
    api/ipf.rst
    api/rasterio.rst
    api/tec.rst
    api/run.rst

.. toctree::
    :maxdepth: 1
    :caption: Data preparation and evaluation

    api/prepare.rst
    api/select.rst
    api/evaluate.rst
    api/visualize.rst
    api/util.rst

.. toctree::
    :maxdepth: 1
    :caption: Defining a groundwater model

    api/mf6.rst
    api/wq.rst
    api/flow.rst
